<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Languages Language Lines
    |--------------------------------------------------------------------------
    */

    'Create' => 'Create New Language',
    'Update' => 'Update',
    'DefaultData' => 'Default Data',
    'Name' => 'Name',
    'Symbol' => 'Symbol',
    'Direction' => 'Direction',
];
