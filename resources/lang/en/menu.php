<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    */

    'Statistics' => 'Statistics',
    'Dashboard' => 'Dashboard',
    'Content' => 'Content',
    'Partners' => 'Partners',
    'Customers' => 'Customers',
    'Stores' => 'Stores',
    'Advertisements' => 'Advertisements',
    'Recommendations' => 'Recommendations',
    'ApiTokens' => 'Api Tokens',
    'World' => 'World',
    'Countries' => 'Countries',
    'Cities' => 'Cities',
    'Districts' => 'Districts',
    'Languages' => 'Languages',
    'AdminSettings' => 'Admin Settings',
    'AdminUsers' => 'Admin Users',

];
