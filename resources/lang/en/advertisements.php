<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Advertisements Language Lines
    |--------------------------------------------------------------------------
    */

    'DefaultData' => 'Default Data',
    'Name' => 'Name',
    'Description' => 'Description',
    'TypeId' => 'Type Id',
    'HashId' => 'Hash Id',
    'Partner' => 'Partner',
    'City' => 'City',
    'Store' => 'Store',
    'Code' => 'Code',
    'Value' => 'Value',
    'ValueType' => 'Value Type',
    'StartAt' => 'Start At',
    'EndAt' => 'End At',
    'FreeDelivery' => 'Free Delivery',
    'Coupon' => 'Coupon',
    'Gift' => 'Gift',
    '1' => 'Free Delivery',
    '2' => 'Coupon',
    '3' => 'Gift',
    'PleaseEnterName' => 'Please Enter Name',
];
