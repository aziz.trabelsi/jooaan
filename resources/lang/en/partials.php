<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Partials Language Lines
    |--------------------------------------------------------------------------
    */

    'Success' => 'Success',
    'GoodJob' => 'Good job!',
    'Error' => 'Error!',
    'Next' => 'Next',
    'Previous' => 'Previous',
    'Save' => 'Save',
    'Settings' => 'Settings',
    'Edit' => 'Edit',
    'Delete' => 'Delete',
    'AreYouSure' => 'Are you sure?',
    'YouWillNotBeAble' => 'You will not be able to recover this imaginary file!',
    'NoCancel' => 'No, cancel plx!',
    'YesDeleteIt' => 'Yes, delete it!',
    'Deleted' => 'Deleted',
    'CannotbeDeleted' => 'Can not be Deleted',
    'Cancelled' => 'Cancelled',
    'YourImaginaryFileIsSafe' => 'Your imaginary file is safe :)',
    'DataSavedSuccussfully' => 'Data Saved Succussfully',
    'DeletedSuccessfully' => 'Deleted successfully',
    'DataResult' => 'Data Result',
    'FormData' => 'Form Data',
    'Close' => 'Close',
    'Search' => 'Search',
    'Preview' => 'Preview',
    'SearchModel' => 'Search Model',
    'PleaseChoose' => 'Please Choose',
    'ThisFieldIsRequired' => 'This field is required',
];
