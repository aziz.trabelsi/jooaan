<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Api Tokens Language Lines
    |--------------------------------------------------------------------------
    */

    'Create' => 'Create New Token',
    'Update' => 'Update',
    'DefaultData' => 'Default Data',
    'Partner' => 'Partner',
];
