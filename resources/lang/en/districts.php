<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Districts Language Lines
    |--------------------------------------------------------------------------
    */

    'Create' => 'Create New District',
    'Update' => 'Update',
    'DefaultData' => 'Default Data',
    'City' => 'City',
    'Name' => 'Name',
    'PleaseEnterName' => 'Please Enter Name',
];
