<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Stores Language Lines
    |--------------------------------------------------------------------------
    */

    'Create' => 'Create New Store',
    'Update' => 'Update',
    'DefaultData' => 'Default Data',
    'Country' => 'Country',
    'Name' => 'Name',
    'Logo' => 'Logo',
    'PleaseEnterName' => 'Please Enter Name',
];
