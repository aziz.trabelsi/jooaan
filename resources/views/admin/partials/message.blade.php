@if(session()->has('success'))
<script type="text/javascript">
    $(document).ready(function () {
        swal("{{__('partials.GoodJob')}}", "{{session('success')}}", "success");
    });
</script>
@endif

@if(session()->has('error'))
<script type="text/javascript">
    $(document).ready(function () {
        var errors = <?= json_encode(session('error')); ?>;
        console.log(errors);
        var el;
        for (var i in errors) {
            el = document.createElement("span");
            var node = document.createTextNode(errors[i]);
            el.style.cssText = 'color:red';
            el.appendChild(node);
        }

		swal({
			title: "{{Lang::get('partials.Error')}}",
			content: {
				element: el,
			}
		});
	
    });
</script>
@endif