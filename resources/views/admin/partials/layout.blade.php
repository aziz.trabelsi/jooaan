<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template.">
  <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
  <meta name="author" content="PIXINVENT">
  <title>@yield('title')</title>
  <link rel="apple-touch-icon" href="{{asset('asset/app-assets/images/ico/apple-icon-120.png')}}">
  <link rel="shortcut icon" type="image/x-icon" href="{{asset('asset/app-assets/images/ico/favicon.ico')}}">
  <link href="{{asset('https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,400,500,700')}}"
  rel="stylesheet">
  <!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('asset/app-assets/css/vendors.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('asset/app-assets/vendors/css/ui/prism.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('asset/app-assets/vendors/css/extensions/sweetalert.css')}}">
  <!-- END VENDOR CSS-->
  <!-- BEGIN ROBUST CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('asset/app-assets/css/app.css')}}">
  <!-- END ROBUST CSS-->
  <!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('asset/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('asset/app-assets/css/core/colors/palette-gradient.css')}}">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('asset/assets/css/style.css')}}">
  <!-- END Custom CSS-->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @stack('css')
</head>
<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar"
data-open="click" data-menu="vertical-menu" data-col="2-columns">

  @include('admin.partials.navbar')
  @include('admin.partials.menu')
  
  <div class="app-content content">
    <div class="content-wrapper">

      @yield('page-header')

      <div class="content-body">

        @yield('content')

      </div>
    </div>
  </div>

  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <footer class="footer footer-static footer-dark">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2018 <a class="text-bold-800 grey darken-2" href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent"
        target="_blank">PIXINVENT </a>, All rights reserved. </span>
      <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span>
    </p>
  </footer>
  <!-- BEGIN VENDOR JS-->
<script src="{{asset('asset/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
<script type="text/javascript" src="{{asset('asset/app-assets/vendors/js/ui/prism.min.js')}}"></script>
<script src="{{asset('asset/app-assets/vendors/js/extensions/sweetalert.min.js')}}" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN ROBUST JS-->
<script src="{{asset('asset/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
<script src="{{asset('asset/app-assets/js/core/app.js')}}" type="text/javascript"></script>
<script src="{{asset('asset/app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
  <!-- END ROBUST JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <!-- END PAGE LEVEL JS-->
  @message
  @stack('js')
</body>
</html>