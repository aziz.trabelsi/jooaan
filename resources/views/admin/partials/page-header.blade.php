<div class="content-header row">
  <div class="content-header-left col-md-6 col-12">
    <h3 class="content-header-title">{{$pageTitle}} </h3>
    <div class="row breadcrumbs-top">
      <div class="breadcrumb-wrapper col-12">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('menu.Dashboard')}}</a></li>

          @foreach ($pagesBreadcrumb as $item)
          <li class="breadcrumb-item"><a href="{{$item['link']}}">{{$item['title']}}</a></li>
          @endforeach

          <li class="breadcrumb-item active">{{$currentPageTitle}}</li>
        </ol>
      </div>
    </div>
  </div>
  <div class="content-header-right col-md-6 col-12">
    <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
      <div class="btn-group" role="group">

        @isset($haveSearch)
        <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#searchModel"><i class="fa fa-search-plus"></i></button>
        @endisset

        @isset($haveCache)
        <a class="btn btn-outline-primary" href="{{$linkCache}}"><i class="fa fa-refresh"></i></a>
        @endisset

        @isset($linkPageCreate)
        <a class="btn btn-outline-success" href="{{$linkPageCreate}}"><i class="fa fa-plus"></i></a>
        @endisset

      </div>
    </div>
  </div>
  <div class="content-header-lead col-12 mt-2">

  </div>
</div>