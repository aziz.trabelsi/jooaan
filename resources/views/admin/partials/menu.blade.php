<div class="main-menu menu-fixed menu-dark menu-accordion    menu-shadow " data-scroll-to-active="true">
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

      <li class="navigation-header">
        <span data-i18n="nav.category.layouts">{{__('menu.Statistics')}}</span>
        <i class="ft-more-horizontal ft-minus" data-toggle="tooltip" data-placement="right"
          data-original-title="Layouts"></i>
      </li>

      <li class="nav-item {{(request()->is('dashboard*'))?'active':''}}">
        <a href="{{route('dashboard')}}">
          <i class="fa fa-dashboard"></i>
          <span class="menu-title" data-i18n="nav.dashboard.main">{{__('menu.Dashboard')}}</span>
          <span class="badge badge badge-success float-right">New</span>
        </a>
      </li>

      <li class="navigation-header">
        <span data-i18n="nav.category.layouts">{{__('menu.Content')}}</span>
        <i class="ft-more-horizontal ft-minus" data-toggle="tooltip" data-placement="right"
          data-original-title="Layouts"></i>
      </li>

      <li class="nav-item {{(request()->is('partners*'))?'active':''}}">
        <a href="{{route('partners')}}">
          <i class="fa fa-briefcase"></i>
          <span class="menu-title" data-i18n="nav.partners.main">{{__('menu.Partners')}}</span>
        </a>
      </li>

      <li class="nav-item {{(request()->is('customers*'))?'active':''}}">
        <a href="{{route('customers')}}">
          <i class="fa fa-users"></i>
          <span class="menu-title" data-i18n="nav.customers.main">{{__('menu.Customers')}}</span>
        </a>
      </li>

      <li class="nav-item {{(request()->is('stores*'))?'active':''}}">
        <a href="{{route('stores')}}">
          <i class="fa fa-shopping-cart"></i>
          <span class="menu-title" data-i18n="nav.stores.main">{{__('menu.Stores')}}</span>
        </a>
      </li>

      <li class="nav-item {{(request()->is('advertisements*'))?'active':''}}">
        <a href="{{route('advertisements')}}">
          <i class="fa fa-bullhorn"></i>
          <span class="menu-title" data-i18n="nav.advertisements.main">{{__('menu.Advertisements')}}</span>
        </a>
      </li>

      <li class="nav-item {{(request()->is('recommendations*'))?'active':''}}">
        <a href="{{route('recommendations')}}">
          <i class="fa fa-thumbs-up"></i>
          <span class="menu-title" data-i18n="nav.recommendations.main">{{__('menu.Recommendations')}}</span>
        </a>
      </li>

      <li class="nav-item {{(request()->is('api-tokens*'))?'active':''}}">
        <a href="{{route('apiTokens')}}">
          <i class="fa fa-unlock"></i>
          <span class="menu-title" data-i18n="nav.apiTokens.main">{{__('menu.ApiTokens')}}</span>
        </a>
      </li>

      <li class="nav-item {{(request()->is('countries*'))?'active':''}}">
        <a href="#"><i class="fa fa-globe"></i>
          <span class="menu-title" data-i18n="nav.world.main">{{__('menu.World')}}</span>
        </a>
        <ul class="menu-content">
          <li {{(request()->is('countries*'))? "class=active":''}}>
            <a class="menu-item" href="{{route('countries')}}"
              data-i18n="nav.world.countries">{{__('menu.Countries')}}</a>
          </li>
          <li {{(request()->is('cities*'))? "class=active":''}}>
            <a class="menu-item" href="{{route('cities')}}" data-i18n="nav.world.cities">{{__('menu.Cities')}}
            </a>
          </li>
          <li {{(request()->is('districts*'))? "class=active":''}}>
            <a class="menu-item" href="{{route('districts')}}" data-i18n="nav.world.districts">{{__('menu.Districts')}}
            </a>
          </li>
        </ul>
      </li>

      <li class="nav-item {{(request()->is('languages*'))?'active':''}}">
        <a href="{{route('languages')}}">
          <i class="fa fa-language"></i>
          <span class="menu-title" data-i18n="nav.languages.main">{{__('menu.Languages')}}</span>
        </a>
      </li>

      <li class="navigation-header">
        <span data-i18n="nav.category.layouts">{{__('menu.AdminSettings')}}</span>
        <i class="ft-more-horizontal ft-minus" data-toggle="tooltip" data-placement="right"
          data-original-title="Layouts"></i>
      </li>

      <li class="nav-item {{(request()->is('admin-users*'))?'active':''}}">
        <a href="{{route('adminUsers')}}">
          <i class="fa fa-user-secret"></i>
          <span class="menu-title" data-i18n="nav.adminusers.main">{{__('menu.AdminUsers')}}</span>
        </a>
      </li>

    </ul>
  </div>
</div>