@extends('admin.partials.layout')
@section('title', __('menu.Recommendations'))

@section('page-header')
@include('admin.partials.page-header',[
'pageTitle'=> __('menu.Recommendations'),
'linkCache'=>'',
'pagesBreadcrumb'=>[],
'currentPageTitle'=> __('menu.Recommendations'),
])
@endsection

@section('content')
<section id="icon-tabs">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{__('partials.DataResult')}}</h4>
                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>{{__('recommendations.Advertise')}}</th>
                                        <th>{{__('recommendations.Customer')}}</th>
                                        {{-- <th>{{__('partials.Settings')}}</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($result as $row)
                                    <tr>
                                        <td>{{@$row->advertise->name}}</td>
                                        <td>{{@$row->customer->name}}</td>
                                        {{-- <td>
                                            <a href="{{route('recommendations.delete',[$row->id])}}"
                                                class="btn btn-sm delete-item btn-outline-danger mb-1">{{__('partials.Delete')}}</a>
                                        </td> --}}
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{$result->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@delete_js
@endsection