@push('css')
<link rel="stylesheet" type="text/css"
    href="{{asset('asset/app-assets/css/plugins/forms/validation/form-validation.css')}}">
@endpush

<!-- Start From -->
<form class="form" action="{{route('adminUsers.store')}}" method="POST" novalidate>
    @csrf
    <input type="hidden" name="user_id" value="{{$user->id}}">
    <div class="form-body">
        <div class="row">

            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">{{__('users.Name')}} <span class="danger">*</span></label>
                    <input type="text" name="name" value="{{$user->name}}" id="name" class="form-control"
                        placeholder="{{__('Users.Name')}}" required
                        data-validation-required-message="{{__('partials.ThisFieldIsRequired')}}">
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="email">{{__('users.Email')}} <span class="danger">*</span></label>
                    <input type="email" name="email" value="{{$user->email}}" id="email"
                        class="form-control" placeholder="{{__('users.Email')}}" required
                        data-validation-required-message="{{__('partials.ThisFieldIsRequired')}}">
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="password">{{__('users.Password')}} <span class="danger">*</span></label>
                    <input type="password" name="password" id="password"
                        class="form-control" placeholder="{{__('users.Password')}}" required
                        data-validation-required-message="{{__('partials.ThisFieldIsRequired')}}">
                </div>
            </div>

        </div>
    </div>

    <div class="form-actions right">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check-square-o"></i> {{__('partials.Save')}}
        </button>
    </div>
</form>
<!-- Start From -->

@push('js')
<script src="{{asset('asset/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}" type="text/javascript">
</script>

<script src="{{asset('asset//app-assets/js/scripts/forms/validation/form-validation.js')}}" type="text/javascript">
</script>
@endpush