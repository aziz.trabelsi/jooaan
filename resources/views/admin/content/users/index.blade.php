@extends('admin.partials.layout')
@section('title', __('menu.AdminUsers'))

@section('page-header')
@include('admin.partials.page-header',[
'pageTitle'=> __('menu.AdminUsers'),
'haveSearch'=>true,
'linkCache'=>'',
'pagesBreadcrumb'=>[],
'currentPageTitle'=> __('menu.AdminUsers'),
'linkPageCreate'=>'admin-users/create'
])
@endsection

@section('content')
<section id="icon-tabs">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{__('partials.DataResult')}}</h4>
                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>{{__('users.Name')}}</th>
                                        <th>{{__('users.Email')}}</th>
                                        <th>{{__('partials.Settings')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($result as $row)
                                    <tr>
                                        <td>{{$row->name}}</td>
                                        <td>{{$row->email}}</td>
                                        <td>
                                            <a href="{{route('adminUsers.update',[$row->id])}}"
                                                class="btn btn-sm btn-outline-info mb-1">{{__('partials.Edit')}}</a>
                                            <a href="{{route('adminUsers.delete',[$row->id])}}"
                                                class="btn btn-sm delete-item btn-outline-danger mb-1">{{__('partials.Delete')}}</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{$result->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@delete_js
<div class="modal fade text-left" id="searchModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel35">{{__('partials.SearchModel')}}</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="GET">
                <div class="modal-body">
                    <fieldset class="form-group floating-label-form-group">
                        <label for="name">{{__('users.Name')}}</label>
                        <input type="text" name="name" value="{{request()->name}}" class="form-control" id="name"
                            placeholder="{{__('Users.PleaseEnterName')}}">
                    </fieldset>
                    <br>
                    <fieldset class="form-group floating-label-form-group">
                        <label for="email">{{__('users.Email')}}</label>
                        <input type="email" name="email" value="{{request()->email}}" class="form-control" id="email"
                            placeholder="{{__('Users.PleaseEnterEmail')}}">
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-outline-primary btn-sm" value="{{__('partials.Search')}}">
                    <input type="reset" class="btn btn-outline-secondary btn-sm" data-dismiss="modal"
                        value="{{__('partials.Close')}}">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection