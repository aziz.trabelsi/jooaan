@push('css')
<link rel="stylesheet" type="text/css"
    href="{{asset('asset/app-assets/css/plugins/forms/validation/form-validation.css')}}">
@endpush

<!-- Start From -->
<form class="form" action="{{route('apiTokens.store')}}" method="POST" novalidate>
    @csrf
    <input type="hidden" name="api_token_id" value="{{$apiToken->id}}">
    <div class="form-body">
        <div class="row">

            <div class="col-md-12">
                <div class="form-group">
                    {!!\App\Http\Controllers\Components\Dropdown::getPartners($apiToken->partner_id, 'required')!!}
                </div>
            </div>

        </div>
    </div>

    <div class="form-actions right">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check-square-o"></i> {{__('partials.Save')}}
        </button>
    </div>
</form>
<!-- Start From -->

@push('js')
<script src="{{asset('asset/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}" type="text/javascript">
</script>

<script src="{{asset('asset//app-assets/js/scripts/forms/validation/form-validation.js')}}" type="text/javascript">
</script>
@endpush