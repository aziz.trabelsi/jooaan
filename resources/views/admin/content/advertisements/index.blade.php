@extends('admin.partials.layout')
@section('title', __('menu.Advertisements'))

@section('page-header')
@include('admin.partials.page-header',[
'pageTitle'=> __('menu.Advertisements'),
'haveSearch'=>true,
'linkCache'=>'',
'pagesBreadcrumb'=>[],
'currentPageTitle'=> __('menu.Advertisements'),
])
@endsection

@section('content')
<section id="icon-tabs">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{__('partials.DataResult')}}</h4>
                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>{{__('advertisements.Name')}}</th>
                                        <th>{{__('advertisements.Partner')}}</th>
                                        <th>{{__('advertisements.Store')}}</th>
                                        <th>{{__('advertisements.StartAt')}}</th>
                                        <th>{{__('advertisements.EndAt')}}</th>
                                        <th>{{__('partials.Settings')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($result as $row)
                                    <tr>
                                        <td>{{$row->name}}</td>
                                        <td>{{@$row->partner->name}}</td>
                                        <td>{{@$row->store->name}}</td>
                                        <td>{{$row->start_at}}</td>
                                        <td>{{$row->end_at}}</td>
                                        <td>
                                            <input type="checkbox" name="single_switch" @if($row->is_active == 1)
                                            checked @endif class="switchBootstrap"
                                            data-url="{{route('advertisements.activated',[$row->id])}}">
                                            <button type="button" class="btn btn-outline-primary btn-sm"
                                                data-toggle="modal" data-target="#previewDetails-{{$row->id}}">
                                                {{__('partials.Preview')}}
                                            </button>
                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="previewDetails-{{$row->id}}"
                                                tabindex="-1" role="dialog" aria-labelledby="myModalLabel-{{$row->id}}"
                                                aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel-{{$row->id}}">
                                                                {{__('partials.Preview')}}</h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>{{__('advertisements.HashId')}}: {{$row->hash_id}}</p>
                                                            <p>{{__('advertisements.Name')}}: {{$row->name}}</p>
                                                            <p>{{__('advertisements.Description')}}: {{$row->description}}</p>
                                                            <p>{{__('advertisements.TypeId')}}: {{$row->type_attr}}</p>
                                                            <p>{{__('advertisements.Partner')}}: {{@$row->partner->name}}</p>
                                                            <p>{{__('advertisements.City')}}: {{$row->city_id}}</p>
                                                            <p>{{__('advertisements.Store')}}: {{@$row->store->name}}</p>
                                                            <p>{{__('advertisements.Code')}}: {{$row->code}}</p>
                                                            <p>{{__('advertisements.Value')}}: {{$row->value}} @if($row->value_type == 1) % @endif</p>
                                                            <p>{{__('advertisements.StartAt')}}: {{$row->start_at}}</p>
                                                            <p>{{__('advertisements.EndAt')}}: {{$row->end_at}}</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button"
                                                                class="btn grey btn-outline-secondary btn-sm"
                                                                data-dismiss="modal">{{__('partials.Close')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- <a href="{{route('advertisements.delete',[$row->id])}}"
                                                class="btn btn-sm delete-item btn-outline-danger">{{__('partials.Delete')}}</a> --}}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{$result->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@delete_js
@toggle_js
<div class="modal fade text-left" id="searchModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel35">{{__('partials.SearchModel')}}</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="GET">
                <div class="modal-body">
                    <fieldset class="form-group floating-label-form-group">
                        <label for="name">{{__('advertisements.Name')}}</label>
                        <input type="text" name="name" value="{{request()->name}}" class="form-control" id="name"
                            placeholder="{{__('advertisements.PleaseEnterName')}}">
                    </fieldset>
                    <br>
                    <fieldset class="form-group floating-label-form-group">
                        {!!\App\Http\Controllers\Components\Dropdown::getPartners(request()->partner_id)!!}
                    </fieldset>
                    <br>
                    <fieldset class="form-group floating-label-form-group">
                        {!!\App\Http\Controllers\Components\Dropdown::getStores(request()->store_id)!!}
                    </fieldset>
                    <br>
                    <fieldset class="form-group floating-label-form-group">
                        {!!\App\Http\Controllers\Components\Dropdown::getAdvertiseTypes(request()->type_id)!!}
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-outline-primary btn-sm" value="{{__('partials.Search')}}">
                    <input type="reset" class="btn btn-outline-secondary btn-sm" data-dismiss="modal"
                        value="{{__('partials.Close')}}">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection