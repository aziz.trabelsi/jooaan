@extends('admin.partials.layout')
@section('title', __('menu.Partners'))

@section('page-header')
@include('admin.partials.page-header',[
'pageTitle'=> __('menu.Partners'),
'pagesBreadcrumb'=>[
    ['title'=> __('menu.Partners'), 'link'=> route('partners')]
],
'currentPageTitle'=> __('partners.Update'),
])
@endsection

@section('content')
<section id="icon-tabs">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                <h4 class="card-title">{{__('partials.FormData')}}</h4>
                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        @include('admin.content.partners.form')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection