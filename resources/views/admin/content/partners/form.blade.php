@push('css')
<link rel="stylesheet" type="text/css"
    href="{{asset('asset/app-assets/css/plugins/forms/validation/form-validation.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('asset/app-assets/vendors/css/forms/selects/select2.min.css')}}">
@endpush

<!-- Start From -->
<form class="form" action="{{route('partners.store')}}" method="POST" novalidate>
    @csrf
    <input type="hidden" name="partner_id" value="{{$partner->id}}">
    <div class="form-body">
        <div class="row">

            <div class="col-md-12">
                <div class="form-group">
                    {!!\App\Http\Controllers\Components\Dropdown::getCountries($partner->countries->pluck('country_id')->toArray(),
                    'required',
                    'multiple')!!}
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">{{__('partners.Name')}} <span class="danger">*</span></label>
                    <input type="text" name="name" value="{{$partner->name}}" id="name" class="form-control"
                        placeholder="{{__('partners.Name')}}" required
                        data-validation-required-message="{{__('partials.ThisFieldIsRequired')}}">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="ios_app_id">{{__('partners.IosAppId')}} <span class="danger">*</span></label>
                    <input type="text" name="ios_app_id" value="{{$partner->ios_app_id}}" id="ios_app_id"
                        class="form-control" placeholder="{{__('partners.IosAppId')}}" required
                        data-validation-required-message="{{__('partials.ThisFieldIsRequired')}}">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="android_app_id">{{__('partners.AndroidAppId')}} <span class="danger">*</span></label>
                    <input type="text" name="android_app_id" value="{{$partner->android_app_id}}" id="android_app_id"
                        class="form-control" placeholder="{{__('partners.AndroidAppId')}}" required
                        data-validation-required-message="{{__('partials.ThisFieldIsRequired')}}">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="color_code">{{__('partners.ColorCode')}} <span class="danger">*</span></label>
                    <input type="text" name="color_code" value="{{$partner->color_code}}" id="color_code"
                        class="form-control" placeholder="{{__('partners.ColorCode')}}" required
                        data-validation-required-message="{{__('partials.ThisFieldIsRequired')}}">
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group upload-result">
                    <label for="android_app_id">{{__('partners.Logo')}} <span class="danger">*</span></label>
                    <input type="file" name="file" class="form-control">
                </div>

                <div class="progress" id="upload-loading">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar"
                        aria-valuemax="100" style="width:0%">
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="form-actions right">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check-square-o"></i> {{__('partials.Save')}}
        </button>
    </div>
</form>
<!-- Start From -->

@push('js')
<script src="{{asset('asset/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}" type="text/javascript">
</script>

<script src="{{asset('asset//app-assets/js/scripts/forms/validation/form-validation.js')}}" type="text/javascript">
</script>

<script src="{{asset('asset/app-assets/vendors/js/forms/select/select2.full.min.js')}}" type="text/javascript">
</script>

<script>
    $(".select2").select2();

        /* Upload File */
        $('#upload-loading').hide();
        $('input[type=file]').on('change', function (e) {
            $('#upload-loading .progress-bar').width('0%');
            $('#upload-loading').show();
            var files;
            var file_data = $(this).prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            files = e.target.files;
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{route('upload.file')}}", // point to server-side PHP script 
                dataType: 'json', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                // this part is progress bar
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('#upload-loading .progress-bar').width(percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                success: function (response) {
                    if (response.status === 'ok') {
                        $('#upload-loading').hide();
                        $('.upload-result').prepend('<input type="hidden" name="logo" value="' + response.file + '"  />');
                    } else {
                        $('.upload-result').prepend('<div class="alert alert-danger alert-dismissible" role="alert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">أ—</span></button>' +
                                '' + response.data + '.</div>');
                        $('#upload-loading').hide();
                    }
                }
            });
        });

</script>

@endpush