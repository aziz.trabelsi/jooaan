@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('asset/app-assets/css/plugins/forms/wizard.css')}}">
@endpush

<!-- Start From -->
<form action="{{route('stores.store')}}" method="POST" class="steps-validation wizard-circle">
    @csrf
    <input type="hidden" name="store_id" value="{{$store->id}}">
    <!-- Default Data -->
    <h6>{{__('stores.DefaultData')}}</h6>
    <fieldset>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!!\App\Http\Controllers\Components\Dropdown::getCountries($store->country_id, 'required')!!}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group upload-result">
                    <label for="android_app_id">{{__('stores.Logo')}} <span class="danger">*</span></label>
                    <input type="file" name="file" class="form-control">
                </div>

                <div class="progress" id="upload-loading">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar"
                        aria-valuemax="100" style="width:0%">
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <!-- Languages -->
    @foreach ($languages as $language)
    <h6>{{$language->name}}</h6>
    <fieldset>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">
                        {{__('stores.Name')}}
                        <span class="danger">*</span>
                    </label>
                    <input type="text" name="languages[{{$language->id}}]" value="{{@$storeLanguage[$language->id]}}"
                        class="form-control required" id="name">
                </div>
            </div>
        </div>
    </fieldset>
    @endforeach
</form>
<!-- Start From -->

@push('js')
<script src="{{asset('asset/app-assets/vendors/js/extensions/jquery.steps.min.js')}}" type="text/javascript"></script>

<script src="{{asset('asset/app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}" type="text/javascript">
</script>

@include('admin.partials.wizard_form_script')

<script>
    /* Upload File */
            $('#upload-loading').hide();
            $('input[type=file]').on('change', function (e) {
                $('#upload-loading .progress-bar').width('0%');
                $('#upload-loading').show();
                var files;
                var file_data = $(this).prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);
                files = e.target.files;
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{route('upload.file')}}", // point to server-side PHP script 
                    dataType: 'json', // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    // this part is progress bar
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('#upload-loading .progress-bar').width(percentComplete + '%');
                            }
                        }, false);
                        return xhr;
                    },
                    success: function (response) {
                        if (response.status === 'ok') {
                            $('#upload-loading').hide();
                            $('.upload-result').prepend('<input type="hidden" name="logo" value="' + response.file + '"  />');
                        } else {
                            $('.upload-result').prepend('<div class="alert alert-danger alert-dismissible" role="alert">' +
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">أ—</span></button>' +
                                    '' + response.data + '.</div>');
                            $('#upload-loading').hide();
                        }
                    }
                });
            });
    
</script>
@endpush