<?php

// api_tokens : id, partner_id, user_id, token, is_active

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes(['register' => false]);

Route::group(['namespace' => 'Admin', 'middleware' => ['auth', 'get.languages']], function () {
    //Partials
    Route::post('upload-file', 'PartialsController@uploadFile')->name('upload.file');
    Route::get('get-cities/{country_id}', 'PartialsController@getCities')->name('get.cities');
    //Dashboard
    Route::get('/', 'DashboardController@index')->name('dashboard');
    //Partners
    Route::get('partners', 'PartnersController@index')->name('partners');
    Route::get('partners/create', 'PartnersController@create')->name('partners.create');
    Route::get('partners/update/{partner}', 'PartnersController@update')->name('partners.update');
    Route::post('partners/store', 'PartnersController@store')->name('partners.store');
    Route::get('partners/delete/{partner}', 'PartnersController@delete')->name('partners.delete');
    //Customers
    Route::get('customers', 'CustomersController@index')->name('customers');
    Route::get('customers/delete/{customer}', 'CustomersController@delete')->name('customers.delete');
    //Stores
    Route::get('stores', 'StoresController@index')->name('stores');
    Route::get('stores/create', 'StoresController@create')->name('stores.create');
    Route::get('stores/update/{store}', 'StoresController@update')->name('stores.update');
    Route::post('stores/store', 'StoresController@store')->name('stores.store');
    Route::get('stores/delete/{store}', 'StoresController@delete')->name('stores.delete');
    //Advertisements
    Route::get('advertisements', 'AdvertisementsController@index')->name('advertisements');
    Route::get('advertisements/activated/{advertise}', 'AdvertisementsController@activated')->name('advertisements.activated');
    // Route::get('advertisements/delete/{advertise}', 'AdvertisementsController@delete')->name('advertisements.delete');
    //Recommendations
    Route::get('recommendations', 'RecommendationsController@index')->name('recommendations');
    // Route::get('recommendations/delete/{recommend}', 'RecommendationsController@delete')->name('recommendations.delete');
    //Api Tokens
    Route::get('api-tokens', 'ApiTokensController@index')->name('apiTokens');
    Route::get('api-tokens/create', 'ApiTokensController@create')->name('apiTokens.create');
    Route::post('api-tokens/store', 'ApiTokensController@store')->name('apiTokens.store');
    Route::get('api-tokens/activated/{apiToken}', 'ApiTokensController@activated')->name('apiTokens.activated');
    /**
     * World
     */
    //Countries
    Route::get('countries', 'CountriesController@index')->name('countries');
    Route::get('countries/create', 'CountriesController@create')->name('countries.create');
    Route::get('countries/update/{country}', 'CountriesController@update')->name('countries.update');
    Route::post('countries/store', 'CountriesController@store')->name('countries.store');
    Route::get('countries/activated/{country}', 'CountriesController@activated')->name('countries.activated');
    Route::get('countries/delete/{country}', 'CountriesController@delete')->name('countries.delete');
    //Cities
    Route::get('cities', 'CitiesController@index')->name('cities');
    Route::get('cities/create', 'CitiesController@create')->name('cities.create');
    Route::get('cities/update/{city}', 'CitiesController@update')->name('cities.update');
    Route::post('cities/store', 'CitiesController@store')->name('cities.store');
    Route::get('cities/activated/{city}', 'CitiesController@activated')->name('cities.activated');
    Route::get('cities/delete/{city}', 'CitiesController@delete')->name('cities.delete');
    //Districts
    Route::get('districts', 'DistrictsController@index')->name('districts');
    Route::get('districts/create', 'DistrictsController@create')->name('districts.create');
    Route::get('districts/update/{district}', 'DistrictsController@update')->name('districts.update');
    Route::post('districts/store', 'DistrictsController@store')->name('districts.store');
    Route::get('districts/delete/{district}', 'DistrictsController@delete')->name('districts.delete');
    //Languages
    Route::get('languages', 'LanguagesController@index')->name('languages');
    Route::get('languages/create', 'LanguagesController@create')->name('languages.create');
    Route::get('languages/update/{language}', 'LanguagesController@update')->name('languages.update');
    Route::post('languages/store', 'LanguagesController@store')->name('languages.store');
    Route::get('languages/delete/{language}', 'LanguagesController@delete')->name('languages.delete');
    //Admin Users
    Route::get('admin-users', 'AdminUsersController@index')->name('adminUsers');
    Route::get('admin-users/create', 'AdminUsersController@create')->name('adminUsers.create');
    Route::get('admin-users/update/{user}', 'AdminUsersController@update')->name('adminUsers.update');
    Route::post('admin-users/store', 'AdminUsersController@store')->name('adminUsers.store');
    Route::get('admin-users/delete/{user}', 'AdminUsersController@delete')->name('adminUsers.delete');

});
