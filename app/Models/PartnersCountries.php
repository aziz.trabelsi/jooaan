<?php

namespace App\Models;

use App\Models\HasValidation;
use Illuminate\Foundation\Auth\User as Authenticatable;

class PartnersCountries extends Authenticatable
{
    use HasValidation;

    protected $table = "partners_countries";
    protected $guarded = ['id'];
    public $timestamps = false;
    public $rules = [
        'partner_id' => 'required',
        'country_id' => 'required',
    ];

    public function partner()
    {
        return $this->belongsTo(Partners::class, 'partner_id');
    }

    public function country()
    {
        return $this->belongsTo(Countries::class, 'country_id');
    }

}
