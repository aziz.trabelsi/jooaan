<?php

namespace App\Models;

use App\Models\HasValidation;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Advertisements extends Authenticatable
{
    use HasValidation;
    use TraitLanguage;

    protected $table = "advertisements";
    protected $guarded = ['id'];
    public $timestamps = false;
    protected $lang_columns = [
        'name',
        'description',
        'created_at',
        'updated_at',
    ];
    public $rules = [
        'hash_id' => 'required|unique:advertisements',
        'type_id' => 'required',
        'partner_id' => 'required',
        'store_id' => 'required',
        'code' => 'required',
        'value' => 'required',
        'value_type' => 'required',
        'start_at' => 'required',
        'end_at' => 'required',
    ];

    public function partner()
    {
        return $this->belongsTo(Partners::class, 'partner_id');
    }

    public function store()
    {
        return $this->belongsTo(Stores::class, 'store_id');
    }

    public function scopeisActive($query)
    {
        return $query->where('is_active', true);
    }

    public function scopeisUnActive($query)
    {
        return $query->where('is_active', false);
    }

    public function getLanguages($tableId, $symbol, $field)
    {
        $tableLanguage = AdvertisementsLanguages::join('languages', 'languages.id', 'advertisements_languages.language_id')
            ->whereSymbol($symbol)
            ->whereTableId($tableId)
            ->select('advertisements_languages.name', 'advertisements_languages.description')
            ->first();
        if (is_object($tableLanguage)) {
            return $tableLanguage->$field;
        }
        return '';
    }

    public function getTypeAttrAttribute()
    {
        return __('advertisements.' . $this->type_id);
    }

}
