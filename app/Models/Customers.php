<?php

namespace App\Models;

use App\Models\HasValidation;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customers extends Authenticatable
{
    use HasValidation;

    protected $table = "customers";
    protected $guarded = ['id'];
    public $timestamps = true;
    public $rules = [
        'country_id' => 'required',
        'name' => 'required',
        'phone_number' => 'required|unique:customers',
        'phone_type' => 'required',
        'email' => 'required|unique:customers',
        'push_token' => 'required',
    ];

    public function country()
    {
        return $this->belongsTo(Countries::class, 'country_id');
    }

}
