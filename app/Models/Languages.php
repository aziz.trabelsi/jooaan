<?php

namespace App\Models;

use App\Models\HasValidation;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Languages extends Authenticatable
{
    use HasValidation;

    protected $table = 'languages';
    public $timestamps = true;
    public $rules = [
        'name' => 'required|unique:languages,name',
        'symbol' => 'required|unique:languages,symbol',
        'direction' => 'required',
    ];
    protected $guarded = ['id'];

}
