<?php

namespace App\Models;

use App\Models\HasValidation;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Districts extends Authenticatable
{
    use HasValidation;
    use TraitLanguage;

    protected $table = "districts";
    protected $guarded = ['id'];
    public $timestamps = false;
    protected $lang_columns = [
        'name',
        'created_at',
        'updated_at',
    ];
    public $rules = [
        'city_id' => 'required',
    ];

    public function city()
    {
        return $this->belongsTo(Cities::class, 'city_id');
    }

}
