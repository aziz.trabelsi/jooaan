<?php

namespace App\Models;

use App\Models\HasValidation;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ApiTokens extends Authenticatable
{
    use HasValidation;

    protected $table = "api_tokens";
    protected $guarded = ['id'];
    public $timestamps = false;
    public $rules = [
        'partner_id' => 'required|unique:api_tokens',
        'token' => 'required|unique:api_tokens',
    ];

    public function partner()
    {
        return $this->belongsTo(Partners::class, 'partner_id');
    }

    public function scopeisActive($query){
        return $query->where('is_active', true);
    }

    public function scopeisUnActive($query){
        return $query->where('is_active', false);
    }


}
