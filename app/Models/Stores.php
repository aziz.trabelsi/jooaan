<?php

namespace App\Models;

use App\Models\HasValidation;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Stores extends Authenticatable
{
    use HasValidation;
    use TraitLanguage;

    protected $table = "stores";
    protected $guarded = ['id'];
    public $timestamps = false;
    protected $lang_columns = [
        'name',
        'created_at',
        'updated_at',
    ];
    public $rules = [
        'country_id' => 'required',
        'slug' => 'required|unique:stores',
    ];

    public function country()
    {
        return $this->belongsTo(Countries::class, 'country_id');
    }

    function getLogoUrlAttribute() {
        if ($this->logo != '')
            return url('uploads/' . $this->logo);
        else
            return url('uploads/images/avatar_image.png');
    }

}
