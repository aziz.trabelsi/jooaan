<?php

namespace App\Models;

use App\Models\HasValidation;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Recommendations extends Authenticatable
{
    use HasValidation;

    protected $table = "recommendations";
    protected $guarded = ['id'];
    public $timestamps = false;
    public $rules = [
        'advertise_id' => 'required',
        'customer_id' => 'required',
    ];

    public function advertise()
    {
        return $this->belongsTo(Advertisements::class, 'advertise_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customers::class, 'customer_id');
    }

}
