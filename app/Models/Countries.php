<?php

namespace App\Models;

use App\Models\HasValidation;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Countries extends Authenticatable
{
    use HasValidation;
    use TraitLanguage;

    protected $table = "countries";
    protected $guarded = ['id'];
    public $timestamps = false;
    protected $lang_columns = [
        'name',
        'created_at',
        'updated_at',
    ];
    public $rules = [
        'symbol' => 'required|unique:countries',
    ];

    public function cities()
    {
        return $this->hasMany(Cities::class, 'country_id')->whereIsActive(true);
    }

    public function scopeisActive($query)
    {
        return $query->where('is_active', true);
    }

    public function scopeisUnActive($query)
    {
        return $query->where('is_active', false);
    }

    // public function getLanguages($tableId, $symbol, $field)
    // {
    //     $tableLanguage = CountriesLanguages::join('languages', 'languages.id', 'countries_languages.language_id')
    //         ->whereSymbol($symbol)
    //         ->whereTableId($tableId)
    //         ->select('countries_languages.name')
    //         ->first();
    //     if (is_object($tableLanguage)) {
    //         return $tableLanguage->$field;
    //     }
    //     return '';
    // }

    // public function cities()
    // {
    //     return '';
    //     $cities = Cities::whereCountryId(1)->get();
    //     foreach($cities as $city){
    //         $tableLanguage = CitiesLanguages::join('languages', 'languages.id', 'cities_languages.language_id')
    //             ->whereSymbol($symbol)
    //             ->whereTableId($city->id)
    //             ->select('cities_languages.name')
    //             ->first();
    //         if (is_object($tableLanguage)) {
    //             return $tableLanguage->$field;
    //         }
    //     }

    //     return '';
    // }

}
