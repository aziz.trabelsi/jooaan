<?php

namespace App\Http\Controllers\Components;

class Dropdown
{

    public static function getCountries($selected = '', $required = '', $multiple = '')
    {
        $_this = new self;
        $star = '';
        if ($required != '') {
            $star = '*';
        }

        if ($multiple != '') {
            $multiple = 'multiple';
            $name = 'country_id[]';
        } else {
            $multiple = '';
            $name = 'country_id';
        }

        $html = '<label>' . __('menu.Countries') . '<span class="danger">' . $star . '</span></label>
                <select name="' . $name . '" class="form-control select2" ' . $multiple . ' aria-invalid="false" ' . $required . '>';
        if ($multiple == '') {
            $html .= ' <option value="">' . __('partials.PleaseChoose') . '</option>';
        }

        $result = \App\Models\Countries::orderBy('id', 'ASC')->get();

        foreach ($result as $row) {
            $html .= '<option ' . $_this->selected($row->id, $selected) . ' value="' . $row->id . '">' . $row->name . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public static function getCities($countryId, $selected = '', $required = '', $multiple = '')
    {
        $_this = new self;
        $star = '';
        if ($required != '') {
            $star = '*';
        }

        if ($multiple != '') {
            $multiple = 'multiple';
            $name = 'city_id[]';
        } else {
            $multiple = '';
            $name = 'city_id';
        }

        $html = '<label>' . __('menu.Cities') . '<span class="danger">' . $star . '</span></label>
                <select name="' . $name . '" class="form-control select2" ' . $multiple . ' aria-invalid="false" ' . $required . '>';
        if ($multiple == '') {
            $html .= ' <option value="">' . __('partials.PleaseChoose') . '</option>';
        }

        $result = \App\Models\Cities::whereCountryId($countryId)->orderBy('id', 'ASC')->get();

        foreach ($result as $row) {
            $html .= '<option ' . $_this->selected($row->id, $selected) . ' value="' . $row->id . '">' . $row->name . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public static function getPartners($selected = '', $required = '')
    {
        $_this = new self;
        $star = '';
        if ($required != '') {
            $star = '*';
        }

        $html = '<label>' . __('menu.Partners') . '<span class="danger">' . $star . '</span></label>
                <select name="partner_id" class="form-control select2" aria-invalid="false" ' . $required . '>';

        $html .= ' <option value="">' . __('partials.PleaseChoose') . '</option>';

        $result = \App\Models\Partners::orderBy('id', 'ASC')->get();

        foreach ($result as $row) {
            $html .= '<option ' . $_this->selected($row->id, $selected) . ' value="' . $row->id . '">' . $row->name . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public static function getStores($selected = '', $required = '')
    {
        $_this = new self;
        $star = '';
        if ($required != '') {
            $star = '*';
        }

        $html = '<label>' . __('menu.Stores') . '<span class="danger">' . $star . '</span></label>
                <select name="store_id" class="form-control select2" aria-invalid="false" ' . $required . '>';

        $html .= ' <option value="">' . __('partials.PleaseChoose') . '</option>';

        $result = \App\Models\Stores::orderBy('id', 'ASC')->get();

        foreach ($result as $row) {
            $html .= '<option ' . $_this->selected($row->id, $selected) . ' value="' . $row->id . '">' . $row->name . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public static function getAdvertiseTypes($selected = '', $required = '')
    {
        $_this = new self;
        $star = '';
        if ($required != '') {
            $star = '*';
        }

        $html = '<label>' . __('advertisements.TypeId') . '<span class="danger">' . $star . '</span></label>
        <select name="type_id" class="form-control select2" aria-invalid="false" ' . $required . '>';

        $html .= ' <option value="">' . __('partials.PleaseChoose') . '</option>';
        $html .= '<option ' . $_this->selected('1', $selected) . ' value="1">' . __('advertisements.FreeDelivery') . '</option>';
        $html .= '<option ' . $_this->selected('2', $selected) . ' value="2">' . __('advertisements.Coupon') . '</option>';
        $html .= '<option ' . $_this->selected('3', $selected) . ' value="3">' . __('advertisements.Gift') . '</option>';
        $html .= '</select><div class="help-block"></div></div>';
        return $html;
    }

    private function selected($option1, $option2)
    {
        if (is_array($option2)) {
            if (in_array($option1, $option2)) {
                return "selected='selected'";
            }

        } else
        if ($option1 == $option2) {
            return "selected='selected'";
        }
    }

}
