<?php

namespace App\Http\Controllers\Admin;

use App\Models\Recommendations;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecommendationsController extends Controller
{

    public function __CONSTRUCT()
    {
        view()->share('crudName', __('menu.Recommendations'));
    }

    public function index(Request $request)
    {
        $data['result'] = Recommendations::orderBy('id', 'DESC');

        if ($request->filled('advertise_id')) {
            $data['result']->whereAdvertiseId($request->advertise_id);
        }

        if ($request->filled('customer_id')) {
            $data['result']->whereCustomerId($request->customer_id);
        }

        $data['result'] = $data['result']->paginate(10);

        return view('admin.content.recommendations.index')->with($data);
    }

    public function delete(Request $request, Recommendations $recommend)
    {
        $recommend->delete();
        $response = new \stdClass();
        $response->status = 'ok';
        $response->message = __('partials.DeletedSuccessfully');
        return response()->json($response);
    }
}
