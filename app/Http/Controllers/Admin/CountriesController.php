<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Countries;
use App\Models\CountriesLanguages;
use DB;
use Illuminate\Http\Request;

class CountriesController extends Controller
{

    protected $_errors;

    public function __CONSTRUCT()
    {
        view()->share('crudName', __('menu.Countries'));
    }

    public function index(Request $request)
    {
        $data['result'] = Countries::orderBy('id', 'DESC');

        if ($request->filled('name')) {
            $data['result']->join('countries_languages', 'countries_languages.table_id', 'countries.id')
                ->where('name', 'LIKE', "%{$request->name}%");
        }

        if ($request->filled('symbol')) {
            $data['result']->whereSymbol($request->symbol);
        }

        $data['result'] = $data['result']->select('countries.*')->paginate(10);

        return view('admin.content.countries.index')->with($data);
    }

    public function create()
    {
        return view('admin.content.countries.create')->with('country', new Countries);
    }

    public function update(Request $request, Countries $country)
    {
        $countryLanguage = CountriesLanguages::whereTableId($country->id)->pluck('name', 'language_id')->toArray();

        return view('admin.content.countries.update')->with(['country' => $country, 'countryLanguage' => $countryLanguage]);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            /**
             * Object Country
             */
            $country = Countries::findOrNew($request->country_id);
            $country->fill($request->only(['symbol']));
            if (!$country->validate()) {
                $this->_errors = $country->errors->all();
                throw new \Exception('Error', 133);
            }
            $country->save();
            /**
             * Array Country Languages
             */
            foreach ($request->languages as $langauge_id => $name) {
                $countryLanguage = CountriesLanguages::firstOrNew(['table_id' => $country->id, 'language_id' => $langauge_id]);
                $countryLanguage->name = $name;
                if (!$countryLanguage->validate()) {
                    $this->_errors = $countryLanguage->errors->all();
                    throw new \Exception('Error', 133);
                }
                $countryLanguage->save();
            }

            DB::commit();
            return redirect()->back()->with('success', __('partials.DataSavedSuccussfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            if ($exception->getCode() == 133) {
                return redirect()->back()->with('error', $this->_errors);
            } else {
                dd($exception);
            }

        }
    }

    public function activated(Request $request, Countries $country)
    {
        if ($country->is_active == 0) {
            $country->is_active = 1;
        } else {
            $country->is_active = 0;
        }

        $country->save();
    }


    public function delete(Request $request, Countries $country)
    {
        $country->delete();
        $response = new \stdClass();
        $response->status = 'ok';
        $response->message = __('partials.DeletedSuccessfully');
        return response()->json($response);
    }
}
