<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cities;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PartialsController extends Controller
{

    public function uploadFile(Request $request)
    {
        $rules = ['file' => 'required|max:4000'];

        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['data'] = $validator->errors()->all();
        } else {
            $destinationPath = 'uploads'; // upload path
            $extension = Input::file('file')->getClientOriginalExtension(); // getting image extension
            $fileName = random_int(1, 5000) * (float) microtime() . '.' . $extension; // renameing image

            Input::file('file')->move($destinationPath, $fileName); // uploading file to given path
            //            $thumb=  Helpers::createThumb('./'.$destinationPath, $fileName, './uploads/thumbs');
            $data['status'] = 'ok';
            $data['data'] = './' . $destinationPath . '/' . $fileName;
            $data['file'] = $fileName;
        }
        return response()->json($data);
    }

    public function getCities(Request $request)
    {
        $result = Cities::whereCountryId($request->country_id)->orderBy('id', 'ASC');
        $result = $result->get()->map(function ($item) {
            $item = $item->only(['id', 'name']);
            return $item;
        });
        return response()->json($result);
    }
}
