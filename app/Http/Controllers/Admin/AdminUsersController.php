<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminUsersController extends Controller
{

    protected $_errors;

    public function __CONSTRUCT()
    {
        view()->share('crudName', __('menu.AdminUsers'));
    }

    public function index(Request $request)
    {
        $data['result'] = User::orderBy('id', 'DESC');

        if ($request->filled('name')) {
            $data['result']->whereName($request->name);
        }

        if ($request->filled('email')) {
            $data['result']->whereEmail($request->email);
        }

        $data['result'] = $data['result']->paginate(10);

        return view('admin.content.users.index')->with($data);
    }

    public function create()
    {
        return view('admin.content.users.create')->with('user', new User);
    }

    public function update(Request $request, User $user)
    {
        return view('admin.content.users.update')->with('user', $user);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            /**
             * Object User
             */
            $user = User::findOrNew($request->user_id);
            $request->merge(['password' => Hash::make($request->password)]);
            $user->fill($request->only(['name', 'email', 'password']));
            if (!$user->validate()) {
                $this->_errors = $user->errors->all();
                throw new \Exception('Error', 133);
            }
            $user->save();

            DB::commit();
            return redirect()->back()->with('success', __('partials.DataSavedSuccussfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            if ($exception->getCode() == 133) {
                return redirect()->back()->with('error', $this->_errors);
            } else {
                dd($exception);
            }

        }
    }

    public function delete(Request $request, User $user)
    {
        $user->delete();
        $response = new \stdClass();
        $response->status = 'ok';
        $response->message = __('partials.DeletedSuccessfully');
        return response()->json($response);
    }
}
