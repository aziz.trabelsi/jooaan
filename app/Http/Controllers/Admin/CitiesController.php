<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cities;
use App\Models\CitiesLanguages;
use DB;
use Illuminate\Http\Request;

class CitiesController extends Controller
{

    protected $_errors;

    public function __CONSTRUCT()
    {
        view()->share('crudName', __('menu.Cities'));
    }

    public function index(Request $request)
    {
        $data['result'] = Cities::orderBy('id', 'DESC');

        if ($request->filled('name')) {
            $data['result']->join('cities_languages', 'cities_languages.table_id', 'cities.id')
                ->where('name', 'LIKE', "%{$request->name}%");
        }

        $data['result'] = $data['result']->select('cities.*')->paginate(10);

        return view('admin.content.cities.index')->with($data);
    }

    public function create()
    {
        return view('admin.content.cities.create')->with('city', new Cities);
    }

    public function update(Request $request, Cities $city)
    {
        $cityLanguage = CitiesLanguages::whereTableId($city->id)->pluck('name', 'language_id')->toArray();

        return view('admin.content.cities.update')->with(['city' => $city, 'cityLanguage' => $cityLanguage]);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            /**
             * Object City
             */
            $city = Cities::findOrNew($request->city_id);
            $city->fill($request->only(['country_id']));
            if (!$city->validate()) {
                $this->_errors = $city->errors->all();
                throw new \Exception('Error', 133);
            }
            $city->save();
            /**
             * Array City Languages
             */
            foreach ($request->languages as $langauge_id => $name) {
                $cityLanguage = CitiesLanguages::firstOrNew(['table_id' => $city->id, 'language_id' => $langauge_id]);
                $cityLanguage->name = $name;
                if (!$cityLanguage->validate()) {
                    $this->_errors = $cityLanguage->errors->all();
                    throw new \Exception('Error', 133);
                }
                $cityLanguage->save();
            }

            DB::commit();
            return redirect()->back()->with('success', __('partials.DataSavedSuccussfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            if ($exception->getCode() == 133) {
                return redirect()->back()->with('error', $this->_errors);
            } else {
                dd($exception);
            }

        }
    }

    public function activated(Request $request, Cities $city)
    {
        if ($city->is_active == 0) {
            $city->is_active = 1;
        } else {
            $city->is_active = 0;
        }

        $city->save();
    }

    public function delete(Request $request, Cities $city)
    {
        $city->delete();
        $response = new \stdClass();
        $response->status = 'ok';
        $response->message = __('partials.DeletedSuccessfully');
        return response()->json($response);
    }
}
