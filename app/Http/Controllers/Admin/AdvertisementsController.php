<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Advertisements;
use Illuminate\Http\Request;

class AdvertisementsController extends Controller
{

    public function __CONSTRUCT()
    {
        view()->share('crudName', __('menu.Advertisement'));
    }

    public function index(Request $request)
    {
        $data['result'] = Advertisements::orderBy('id', 'DESC');

        if ($request->filled('name')) {
            $data['result']->join('advertisements_languages', 'advertisements_languages.table_id', 'advertisements.id')
                ->where('name', 'LIKE', "%{$request->name}%");
        }

        if ($request->filled('partner_id')) {
            $data['result']->wherePartnerId($request->partner_id);
        }

        if ($request->filled('store_id')) {
            $data['result']->whereStoreId($request->store_id);
        }

        if ($request->filled('type_id')) {
            $data['result']->whereTypeId($request->type_id);
        }

        $data['result'] = $data['result']->select('advertisements.*')->paginate(10);

        return view('admin.content.advertisements.index')->with($data);
    }
    
    public function activated(Request $request, Advertisements $advertise){
        if($advertise->is_active == 0)
            $advertise->is_active = 1;
        else
            $advertise->is_active = 0;
        $advertise->save();
    }

    public function delete(Request $request, Advertisements $advertise)
    {
        $advertise->delete();
        $response = new \stdClass();
        $response->status = 'ok';
        $response->message = __('partials.DeletedSuccessfully');
        return response()->json($response);
    }
}
