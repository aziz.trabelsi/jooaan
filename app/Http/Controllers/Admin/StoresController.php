<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Stores;
use App\Models\StoresLanguages;
use DB;
use Illuminate\Http\Request;

class StoresController extends Controller
{

    protected $_errors;

    public function __CONSTRUCT()
    {
        view()->share('crudName', __('menu.Stores'));
    }

    public function index(Request $request)
    {
        $data['result'] = Stores::orderBy('id', 'DESC');

        if ($request->filled('name')) {
            $data['result']->join('stores_languages', 'stores_languages.table_id', 'stores.id')
                ->where('name', 'LIKE', "%{$request->name}%");
        }

        if ($request->filled('country_id')) {
            $data['result']->whereCountryId($request->country_id);
        }

        $data['result'] = $data['result']->select('stores.*')->paginate(10);

        return view('admin.content.stores.index')->with($data);
    }

    public function create()
    {
        return view('admin.content.stores.create')->with('store', new Stores);
    }

    public function update(Request $request, Stores $store)
    {
        $storeLanguage = StoresLanguages::whereTableId($store->id)->pluck('name', 'language_id')->toArray();

        return view('admin.content.stores.update')->with(['store' => $store, 'storeLanguage' => $storeLanguage]);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            /**
             * Object Store
             */
            $firstItem = current($request->languages);
            $store = Stores::findOrNew($request->store_id);
            $store->country_id = $request->country_id;
            $store->slug = str_slug($firstItem);
            if($request->filled('logo')){
                $store->logo = $request->logo;
            }
            if (!$store->validate()) {
                $this->_errors = $store->errors->all();
                throw new \Exception('Error', 133);
            }
            $store->save();
            /**
             * Array Store Languages
             */
            foreach ($request->languages as $langauge_id => $name) {
                $storeLanguage = StoresLanguages::firstOrNew(['table_id' => $store->id, 'language_id' => $langauge_id]);
                $storeLanguage->name = $name;
                if (!$storeLanguage->validate()) {
                    $this->_errors = $storeLanguage->errors->all();
                    throw new \Exception('Error', 133);
                }
                $storeLanguage->save();
            }

            DB::commit();
            return redirect()->back()->with('success', __('partials.DataSavedSuccussfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            if ($exception->getCode() == 133) {
                return redirect()->back()->with('error', $this->_errors);
            } else {
                dd($exception);
            }

        }
    }

    public function delete(Request $request, Stores $store)
    {
        $store->delete();
        $response = new \stdClass();
        $response->status = 'ok';
        $response->message = __('partials.DeletedSuccessfully');
        return response()->json($response);
    }
}
