<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Districts;
use App\Models\DistrictsLanguages;
use DB;
use Illuminate\Http\Request;

class DistrictsController extends Controller
{

    protected $_errors;

    public function __CONSTRUCT()
    {
        view()->share('crudName', __('menu.Districts'));
    }

    public function index(Request $request)
    {
        $data['result'] = Districts::orderBy('id', 'DESC');

        if ($request->filled('name')) {
            $data['result']->join('Districts_languages', 'Districts_languages.table_id', 'Districts.id')
                ->where('name', 'LIKE', "%{$request->name}%");
        }

        $data['result'] = $data['result']->select('districts.*')->paginate(10);

        return view('admin.content.districts.index')->with($data);
    }

    public function create()
    {
        return view('admin.content.districts.create')->with('district', new Districts);
    }

    public function update(Request $request, Districts $district)
    {
        $districtLanguage = DistrictsLanguages::whereTableId($district->id)->pluck('name', 'language_id')->toArray();

        return view('admin.content.districts.update')->with(['district' => $district, 'districtLanguage' => $districtLanguage]);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            /**
             * Object District
             */
            $district = Districts::findOrNew($request->district_id);
            $district->fill($request->only(['city_id']));
            if (!$district->validate()) {
                $this->_errors = $district->errors->all();
                throw new \Exception('Error', 133);
            }
            $district->save();
            /**
             * Array District Languages
             */
            foreach ($request->languages as $langauge_id => $name) {
                $districtLanguage = DistrictsLanguages::firstOrNew(['table_id' => $district->id, 'language_id' => $langauge_id]);
                $districtLanguage->name = $name;
                if (!$districtLanguage->validate()) {
                    $this->_errors = $districtLanguage->errors->all();
                    throw new \Exception('Error', 133);
                }
                $districtLanguage->save();
            }

            DB::commit();
            return redirect()->back()->with('success', __('partials.DataSavedSuccussfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            if ($exception->getCode() == 133) {
                return redirect()->back()->with('error', $this->_errors);
            } else {
                dd($exception);
            }

        }
    }

    public function delete(Request $request, Districts $district)
    {
        $district->delete();
        $response = new \stdClass();
        $response->status = 'ok';
        $response->message = __('partials.DeletedSuccessfully');
        return response()->json($response);
    }
}
