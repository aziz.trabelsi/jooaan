<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Partners;
use App\Models\PartnersCountries;
use DB;
use Illuminate\Http\Request;

class PartnersController extends Controller
{

    protected $_errors;

    public function __CONSTRUCT()
    {
        view()->share('crudName', __('menu.Partners'));
    }

    public function index(Request $request)
    {
        $data['result'] = Partners::orderBy('id', 'DESC');

        if ($request->filled('name')) {
            $data['result']->whereName($request->name);
        }

        if ($request->filled('country_id')) {
            $data['result']->whereCountryId($request->country_id);
        }

        $data['result'] = $data['result']->paginate(10);

        return view('admin.content.partners.index')->with($data);
    }

    public function create()
    {
        return view('admin.content.partners.create')->with('partner', new Partners);
    }

    public function update(Request $request, Partners $partner)
    {
        return view('admin.content.partners.update')->with('partner', $partner);
    }

    public function store(Request $request)
    {

        DB::beginTransaction();
        try {
            // dd($request->all());
            /**
             * Object Partner
             */
            $partner = Partners::findOrNew($request->partner_id);
            $request->request->add(['slug' => str_slug($request->name)]);
            $partner->fill($request->only(['ios_app_id', 'android_app_id', 'slug', 'name', 'logo', 'color_code']));
            if (!$partner->validate()) {
                $this->_errors = $partner->errors->all();
                throw new \Exception('Error', 133);
            }
            $partner->save();

            /**
             * Array Partner Countries
             */
            if ($request->filled('country_id')) {
                foreach ($request->country_id as $country_id) {
                    PartnersCountries::firstOrCreate(['partner_id' => $partner->id, 'country_id' => $country_id]);
                }
            }

            DB::commit();
            return redirect()->back()->with('success', __('partials.DataSavedSuccussfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            if ($exception->getCode() == 133) {
                return redirect()->back()->with('error', $this->_errors);
            } else {
                dd($exception);
            }

        }
    }

    public function delete(Request $request, Partners $partner)
    {
        $partner->delete();
        $response = new \stdClass();
        $response->status = 'ok';
        $response->message = __('partials.DeletedSuccessfully');
        return response()->json($response);
    }
}
