<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Components\API;
use App\Http\Controllers\Controller;
use App\Models\Advertisements;
use App\Models\AdvertisementsLanguages;
use App\Models\Countries;
use App\Models\Customers;
use App\Models\Languages;
use App\Models\Partners;
use DB;
use Illuminate\Http\Request;
use Vinkla\Hashids\Facades\Hashids;

class PartialsController extends Controller
{
    protected $_errors;

    public function getList(Request $request)
    {
        $headerLang = $this->validateLanguageId();
        if ($headerLang === false) {
           return API::apiErrorMessage('lang missing');
        }

        $currentCountry = Api::getCountry($request);
        /**
         * Get Active Countries
         */
        $activeCountries = Countries::isActive()->orderBy('id', 'ASC');
        $activeCountries = $activeCountries->get()->map(function ($item) {
            $item = $item->only(['id', 'name']);
            return $item;
        });
        $data['activeCountries'] = $activeCountries;

        /**
         * Get Active Cities
         */
        $countries = Countries::isActive()->orderBy('id', 'ASC')->get();
        $activeCities = [];
        foreach ($countries as $country) {
            $cities = [];
            foreach ($country->cities as $city) {
                $cities[] = ['cityId' => $city->id, 'name' => $city->name];
            }
            $activeCities = [
                'countryId' => $country->id,
                'cities' => $cities,
            ];

        }
        $data['activeCities'] = $activeCities;

        /**
         * Get Advertisments
         */
        $advertisments = Advertisements::isActive()->orderBy('id', 'ASC');
        $advertisments = $advertisments->get()->map(function ($item) {
            $item->type = $item->type_attr;
            $item->partnerId = $item->partner_id;
            $item->storeName = @$item->store->name;
            $item->storeImage = @$item->store->logo_url;
            $item = $item->only(['type', 'partnerId', 'storeName', 'storeImage', 'code', 'value']);
            return $item;
        });
        $data['advertisments'] = $advertisments;

        /**
         * Get Partners
         */
        $partners = Partners::orderBy('id', 'ASC');
        $partners = $partners->get()->map(function ($item) {
            $item->imageUrl = $item->logo_url;
            $item = $item->only(['id', 'imageUrl', 'name']);
            return $item;
        });
        $data['partners'] = $partners;

        $data['current_country'] = $currentCountry['current_country'];
        $data['current_city'] = $currentCountry['current_city'];

        return API::apiSuccessMessage($data);
    }

    public function addAdvertise(Request $request)
    {
        $apiValidate = API::apiValidate($request, ['type_id', 'partner_id', 'store_id', 'code', 'value', 'value_type', 'start_at', 'end_at', 'languages']);
        if ($apiValidate != null) {
            return $apiValidate;
        }

        DB::beginTransaction();
        try {
            /**
             * Object Advertise
             */
            $advertise = new Advertisements;
            $request->request->add(['hash_id' => Hashids::encode(rand(9999, 999999999))]);
            $advertise->fill($request->only(['hash_id', 'type_id', 'partner_id', 'store_id', 'city_id', 'code', 'value', 'value_type', 'start_at', 'end_at']));
            if (!$advertise->validate('api')) {
                foreach ($advertise->errors->getMessages() as $key => $error) {
                    $errors[] = [$key => $error[0]];
                }
                $this->_errors = $errors;
                throw new \Exception('Error', 133);
            }
            $advertise->save();

            /**
             * Array Advertise Languages
             */
            if ($request->filled('languages')) {
                $languages = Languages::all();
                foreach ($languages as $language) {
                    $requestLanguage = $request->languages[0][$language->symbol];
                    $advertiselanguage = AdvertisementsLanguages::firstOrNew(['table_id' => $advertise->id, 'language_id' => $language->id]);
                    $advertiselanguage->name = $requestLanguage['name'];
                    $advertiselanguage->description = $requestLanguage['description'];
                    if (!$advertiselanguage->validate('api')) {
                        foreach ($advertiselanguage->errors->getMessages() as $key => $error) {
                            $errors[] = [$key => $error[0]];
                        }
                        $this->_errors = $errors;
                        throw new \Exception('Error', 133);
                    }
                    $advertiselanguage->save();
                }
            }

            DB::commit();
            return API::apiSuccessMessage('Data Saved Succussfully');
        } catch (\Exception $exception) {
            DB::rollBack();
            if ($exception->getCode() == 133) {
                return API::apiErrorMessage($this->_errors);
            } else {
                dd($exception);
            }

        }

    }

    public function verifyUser(Request $request)
    {
        $apiValidate = API::apiValidate($request, ['phone_number']);
        if ($apiValidate != null) {
            return $apiValidate;
        }

        $customer = Customers::wherePhoneNumber($request->phone_number)->first();
        if (is_object($customer)) {
            return API::apiSuccessMessage($customer);
        } else {
            return API::apiErrorMessage('Customer Not Found');
        }

    }

    private function validateLanguageId()
    {
        $headers = getallheaders();

        if (!array_has($headers, 'Lang')) {
            return false;
        }

        $LanguageSymbol = array_get($headers, 'Lang');
        \App::setLocale($LanguageSymbol);

        return true;
    }

}
