<?php

use App\Models\Cities;
use App\Models\Languages;
use Illuminate\Database\Seeder;

class WorldDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jsonCities = File::get("database/data/cities.json");
        $dataCities = json_decode($jsonCities);
        foreach ($dataCities as $object) {
            $city = new Cities;
            $city->country_id = 1;
            $city->save();

            foreach (Languages::all() as $language) {
                if ($language->symbol == 'en') {
                    $cityName = $object->name->en;
                }

                if ($language->symbol == 'ar') {
                    $cityName = $object->name->ar;
                }
                CitiesLanguages::firstOrCreate(['table_id' => $city->id, 'language_id' => $language->id, 'name' => $cityName]);
            }
        }

        $jsonDistricts = File::get("database/data/districts.json");
        $dataDistricts = json_decode($jsonDistricts);
        foreach ($dataDistricts as $object) {
            $city = Cities::find($object->city_id);
            if (is_object($city)) {
                $district = new Districts;
                $district->city_id = $object->city_id;
                $district->save();

                foreach (Languages::all() as $language) {
                    if ($language->symbol == 'en') {
                        $districtName = $object->name->en;
                    }

                    if ($language->symbol == 'ar') {
                        $districtName = $object->name->ar;
                    }
                    DistrictsLanguages::firstOrCreate(['table_id' => $district->id, 'language_id' => $language->id, 'name' => $districtName]);
                }
            }
        }

    }
}
