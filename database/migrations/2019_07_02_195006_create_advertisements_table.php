<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdvertisementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('advertisements', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('hash_id', 100)->unique('hash_id');
			$table->integer('type_id')->index('type_id')->comment('1="Free Delivery", 2="Coupon", 3="Gift"');
			$table->integer('partner_id')->index('partner_id');
			$table->integer('city_id')->nullable()->index('city_id');
			$table->integer('store_id')->index('store_id');
			$table->string('code', 100);
			$table->string('value', 100);
			$table->boolean('value_type')->comment('0 ="Price", 1="Percent"');
			$table->dateTime('start_at');
			$table->dateTime('end_at');
			$table->boolean('is_active')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('advertisements');
	}

}
