<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdvertisementsLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('advertisements_languages', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('table_id')->index('table_id');
			$table->integer('language_id')->index('language_id');
			$table->string('name', 100);
			$table->text('description', 65535);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('advertisements_languages');
	}

}
