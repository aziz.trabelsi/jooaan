<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToStoresLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('stores_languages', function(Blueprint $table)
		{
			$table->foreign('table_id', 'stores_languages_ibfk_1')->references('id')->on('stores')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('language_id', 'stores_languages_ibfk_2')->references('id')->on('languages')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('stores_languages', function(Blueprint $table)
		{
			$table->dropForeign('stores_languages_ibfk_1');
			$table->dropForeign('stores_languages_ibfk_2');
		});
	}

}
