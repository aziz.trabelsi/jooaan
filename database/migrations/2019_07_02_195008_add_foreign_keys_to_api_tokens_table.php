<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToApiTokensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('api_tokens', function(Blueprint $table)
		{
			$table->foreign('partner_id', 'api_tokens_ibfk_1')->references('id')->on('partners')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('api_tokens', function(Blueprint $table)
		{
			$table->dropForeign('api_tokens_ibfk_1');
		});
	}

}
