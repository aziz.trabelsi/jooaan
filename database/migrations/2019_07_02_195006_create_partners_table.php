<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePartnersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('partners', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('ios_app_id', 100);
			$table->string('android_app_id', 100);
			$table->string('slug', 100)->unique('slug');
			$table->string('name', 100);
			$table->string('logo', 100);
			$table->string('color_code', 6);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('partners');
	}

}
