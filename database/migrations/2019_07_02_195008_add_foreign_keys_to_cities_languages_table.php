<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCitiesLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cities_languages', function(Blueprint $table)
		{
			$table->foreign('table_id', 'cities_languages_ibfk_1')->references('id')->on('cities')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('language_id', 'cities_languages_ibfk_2')->references('id')->on('languages')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cities_languages', function(Blueprint $table)
		{
			$table->dropForeign('cities_languages_ibfk_1');
			$table->dropForeign('cities_languages_ibfk_2');
		});
	}

}
